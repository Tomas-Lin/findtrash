# 垃圾車Demo  APP

## UI參考IOS APP 垃圾車e點通

![List](https://bitbucket.org/Tomas-Lin/findtrash/src/67ec277d8525f405cf03e3aa0e4976caf181ea84/doc/images/list.jpg?at=master&fileviewer=file-view-default)

![ListOptions](https://bitbucket.org/Tomas-Lin/findtrash/src/67ec277d8525f405cf03e3aa0e4976caf181ea84/doc/images/list_options.jpg?at=master&fileviewer=file-view-default)

![Map](https://bitbucket.org/Tomas-Lin/findtrash/src/67ec277d8525f405cf03e3aa0e4976caf181ea84/doc/images/map.jpg?at=master&fileviewer=file-view-default)

![MapOptions](https://bitbucket.org/Tomas-Lin/findtrash/src/67ec277d8525f405cf03e3aa0e4976caf181ea84/doc/images/map_options.jpg?at=master&fileviewer=file-view-default)

## 參考資料

API :

台北市 ：http://data.taipei.gov.tw/opendata/apply/NewDataContent?oid=021C72FF-591F-4267-AA36-4D0E3A43AAFD

新北市 ：http://data.gov.tw/node/26755


